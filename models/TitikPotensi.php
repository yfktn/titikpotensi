<?php namespace Yfktn\TitikPotensi\Models;

use Model;

/**
 * Model
 */
class TitikPotensi extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'yfktn_titikpotensi_';

    /**
     * @var array Validation rules
     */
    public $rules = [
        'judul'   => ['required'],
        'slug'    => ['required', 'regex:/^[a-z0-9\/\:_\-\*\[\]\+\?\|]*$/i', 'unique:yfktn_titikpotensi_'],
    ];
}
