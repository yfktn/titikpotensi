<?php namespace Yfktn\TitikPotensi\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateYfktnTitikpotensiItemTag extends Migration
{
    public function up()
    {
        Schema::create('yfktn_titikpotensi_item_tag', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('potensi_id')->unsigned()->index();
            $table->string('tag_slug')->index();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('yfktn_titikpotensi_item_tag');
    }
}