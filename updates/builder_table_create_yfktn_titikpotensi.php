<?php namespace Yfktn\TitikPotensi\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateYfktnTitikpotensi extends Migration
{
    public function up()
    {
        Schema::create('yfktn_titikpotensi_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->double('pos_lat', 10, 0);
            $table->double('pos_long', 10, 0);
            $table->string('judul');
            $table->string('slug')->index();
            $table->text('keterangan')->nullable();
            $table->double('zoom_lat', 10, 0)->nullable();
            $table->double('zoom_long', 10, 0)->nullable();
            $table->double('zoom_val', 10, 0);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('yfktn_titikpotensi_');
    }
}