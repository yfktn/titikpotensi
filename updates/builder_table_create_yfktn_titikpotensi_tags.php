<?php namespace Yfktn\TitikPotensi\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateYfktnTitikpotensiTags extends Migration
{
    public function up()
    {
        Schema::create('yfktn_titikpotensi_tags', function($table)
        {
            $table->engine = 'InnoDB';
            $table->string('slug');
            $table->string('nama');
            $table->primary(['slug']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('yfktn_titikpotensi_tags');
    }
}